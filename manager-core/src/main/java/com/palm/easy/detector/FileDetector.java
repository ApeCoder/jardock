package com.palm.easy.detector;

import org.noear.solon.extend.health.detector.AbstractDetector;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 文件检测，检测当前文件的总空间和可用空间
 */
public class FileDetector extends AbstractDetector {

    private String dir="./";
    public FileDetector(){

    }
    public FileDetector(String dir){
        this.dir=dir;
    }
    public FileDetector(File dir){
        if(dir.isFile()){
            dir=dir.getParentFile();
        }
        this.dir=dir.getAbsolutePath();
    }
    @Override
    public String getName() {
        return "file";
    }

    @Override
    public Map<String, Object> getInfo() {
        Map<String, Object> disk = new LinkedHashMap<>();
        File file=new File(dir);
        long total = file.getTotalSpace();
        long free = file.getFreeSpace();
        long used = total - free;

        float ratio = (total > 0L ? used * 100.0F / total : 0F);

        disk.put("total", formatByteSize(total));
        disk.put("free", formatByteSize(free));
        disk.put("used", formatByteSize(used));
        disk.put("ratio", ratio);
        return disk;
    }
}
