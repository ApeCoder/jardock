package com.palm.cloud;

import com.palm.cloud.detector.Detection;
import com.palm.cloud.detector.Detector;
import com.sun.management.OperatingSystemMXBean;
import com.wujiuye.flow.FlowType;
import com.wujiuye.flow.Flower;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.Date;

public class ClientDemoApp {
    public static void main(String[] args) {
        new Thread(()->{
            while (true){
                System.out.println(get());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

        }).start();
        fb(50);
        System.out.println("==============================");

    }
    public static int fb(int i){
        if(i<=1){
            return 1;
        }
        return fb(i-1)+fb(i-2);
    }
    public static Detection get(){

        Detection detection=new Detection();
        detection.setDate(new Date());
        OperatingSystemMXBean  osmxb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        detection.setCpu((int) (osmxb.getProcessCpuLoad()*100));
        MemoryMXBean bean = ManagementFactory.getMemoryMXBean();

        MemoryUsage memoryUsage = bean.getHeapMemoryUsage();
        detection.setJvm(memoryUsage.getUsed()/(1024*1024));

        Runtime runtime = Runtime.getRuntime();
        // SystemUtil.getRuntimeInfo()
        long m1=runtime.totalMemory()- runtime.freeMemory();
        System.out.println(m1/(1024));

        return detection;
    }
}
