require.config({
    baseUrl: "./",
    paths: {
        'vue': 'js/vue',
        Vuex: 'js/lib/vuex.4.0.2.prod',
        HeyUI: 'js/lib/heyui',
        vueRouter: 'js/lib/vue-router.4.0.12.prod',
        G: 'js/lib/hey-global',
        axios: 'js/lib/axios',
        sortablejs: 'js/lib/sortable.min',//CDN 'http://www.itxst.com/package/sortable/Sortable.min',
        vuedraggable: 'js/lib/vue-draggable-next.global.prod',//CDN 'http://www.itxst.com/package/vuedraggable/vuedraggable.umd.min',
        plupload: 'js/lib/plupload.min',
        moxie: 'js/lib/moxie.min',
        vs: 'js/vs',
        echarts: 'js/lib/echarts.min',//'https://cdn.bootcdn.net/ajax/libs/echarts/5.1.2/echarts.min',
        lodash_debounce: 'https://cdn.jsdelivr.net/npm/lodash.debounce@4.0.8/index.min',
        XLSX: 'js/lib/xlsx.full.min',
        wangEditor: 'js/lib/wangEditor.min',
        framework: 'js/framework',
        xterm:'js/xterm/xterm',
        'vue-echarts':'js/lib/vue-echarts'
    },
    shim: {
        vue: {
            exports: 'Vue'
        },
        Vuex: {
            deps: ['vue'],
            exports: 'Vuex'
        },
        vueRouter: {
            deps: ['vue'],
            exports: 'VueRouter'
        },
        HeyUI: {
            deps: ['vue']
        },
        framework: {
            deps: ['vue']
        },
        vuedraggable:{
            deps: ['vue'],
            exports:'VueDraggableNext'
        },
        xterm:{
            deps:['css/xterm.css']
        }
    }
})
