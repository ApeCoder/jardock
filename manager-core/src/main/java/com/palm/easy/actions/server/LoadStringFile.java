package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.exception.BusinessException;
import com.palm.easy.util.StringUtil;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;

@Data
@AllArgsConstructor
public class LoadStringFile implements IServerAction<String> {
    private String path;
    @Override
    public String execute(ServerContext context) {
        Util.checkPath(path);
        File f = new File(ManagerConfiguration.localPath(), path);
        if (!f.exists()) {
          throw new BusinessException("文件不存在");
        }
        return StringUtil.readFrom(f);
    }
}
