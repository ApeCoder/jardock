package com.palm.easy.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务项，服务发现和映射用
 */
public class ServiceItem implements Serializable {
    private String name;
    private String protocol="http";
    private List<String> mappingServers =new ArrayList<>();
    private String mappingPath;
    private String mappingConfig;
    private List<String> urls=new ArrayList<>();

    public ServiceItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public String getMappingConfig() {
        return mappingConfig;
    }

    public void setMappingConfig(String mappingConfig) {
        this.mappingConfig = mappingConfig;
    }

    public List<String> getUrls() {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public List<String> getMappingServers() {
        return mappingServers;
    }

    public void setMappingServers(List<String> mappingServers) {
        this.mappingServers = mappingServers;
    }

    public String getMappingPath() {
        return mappingPath;
    }

    public void setMappingPath(String mappingPath) {
        this.mappingPath = mappingPath;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    public String getMappingServer(){
        if(mappingServers==null){
            return null;
        }
        if(mappingServers.size()>1){
            return name;
        }else if(mappingServers.size()==0){
            return null;
        }else{
            return mappingServers.get(0);
        }
    }
}
