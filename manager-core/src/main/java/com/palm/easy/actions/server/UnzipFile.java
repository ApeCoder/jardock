package com.palm.easy.actions.server;

import com.palm.easy.ManagerConfiguration;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.util.Util;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
import java.io.IOException;
@Data
@AllArgsConstructor
public class UnzipFile implements IServerAction<Boolean> {
    private String path;
    @Override
    public Boolean execute(ServerContext context) {
        Util.checkPath(path);
        File file = new File(ManagerConfiguration.localPath(), path);
        if (file.exists()) {
            if (file.getName().endsWith(".zip")) {
                try {
                    Util.unZipFiles(file, file.getParent());
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
