package com.palm.cloud.detector;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 监测记录
 */
@Data
public class Detection implements Serializable {
    /**
     * 记录时间
     */
    private Date date;

    /**
     * cpu使用率（%）
     */
    private float cpu;
    /**
     * java 内存使用 （M）
     */
    private float jvm;
    /**
     * qps
     */
    private float qps;
    /**
     * 平均响应时间
     */
    private long avgRt;
}
