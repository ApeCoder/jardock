package com.palm.cloud.detector;

import cn.hutool.system.SystemUtil;
import cn.hutool.system.oshi.OshiUtil;
import com.sun.management.OperatingSystemMXBean;
import com.wujiuye.flow.FlowHelper;
import com.wujiuye.flow.FlowType;
import com.wujiuye.flow.Flower;
import org.noear.solon.SolonApp;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import java.util.Date;

public class Detector{
    private static FlowHelper flowHelper;
    private static OperatingSystemMXBean osmxb;
    public static void init(SolonApp app){
        flowHelper = new FlowHelper(FlowType.Second);
        osmxb = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        app.filter((ctx, chain) -> {
            long start = System.currentTimeMillis();
            try {
                chain.doFilter(ctx);
                flowHelper.incrSuccess(System.currentTimeMillis() - start);
            } catch (Throwable e) {
                flowHelper.incrException();
                throw e;
            }
        });
    }
    public static Detection get(){
        Flower flower = flowHelper.getFlow(FlowType.Second);
        Detection detection=new Detection();
        detection.setDate(new Date());
        detection.setAvgRt(flower.avgRt());
        detection.setQps(flower.total());
        detection.setCpu((int) (osmxb.getProcessCpuLoad()*100));
        MemoryMXBean bean = ManagementFactory.getMemoryMXBean();

        MemoryUsage memoryUsage = bean.getHeapMemoryUsage();
        detection.setJvm(memoryUsage.getUsed()/(1024*1024));
        Runtime runtime = Runtime.getRuntime();
       // SystemUtil.getRuntimeInfo()
        long m1=runtime.totalMemory()- runtime.freeMemory();
        System.out.println(m1/(1024));

        return detection;
    }
}
