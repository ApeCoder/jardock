package com.palm.cloud;

import org.noear.solon.Solon;
import org.noear.solon.SolonApp;
import org.noear.solon.Utils;
import org.noear.solon.core.AopContext;
import org.noear.solon.core.Plugin;
import org.noear.solon.core.Props;
import org.noear.solon.core.event.EventBus;
import org.noear.solon.core.message.Message;
import org.noear.solon.core.message.Session;
import org.noear.solon.core.util.PrintUtil;
import org.noear.solon.socketd.SessionFactoryManager;
import org.noear.solon.socketd.SocketD;


import java.net.URI;
import java.net.URL;
import java.util.Properties;

public class EasyClientPlugin implements Plugin {
    private  URL configUrl;
    @Override
    public void start(AopContext context) {
        SolonApp app= Solon.app();
        boolean manager_enable =  app.cfg().getBool("manager.enable",true);
        if(!manager_enable){
            return;
        }
        URL configUrl = Utils.getResource("manager.properties");
        if(configUrl==null){
            return;
        }
        this.configUrl=configUrl;
        context.beanOnloaded(this::beanOnLoaded);

    }
    private void beanOnLoaded(AopContext ctx){
        Props props = new Props(Utils.loadProperties(configUrl));
        String token=props.getProperty("token");
        int timeOut=props.getInt("timeout",30);
        String address="ws://"+props.getProperty("host")+"/client?token="+token;
        PrintUtil.info("准备连接Manager: "+address);
        Session s = SessionFactoryManager.create(URI.create(address), true);
        EasyClient.serverSession=s;
        s.listener(new ClientListener());
        //发送握手包
        try {
            s.sendHeartbeat();
        } catch (Throwable ex) {
            EventBus.push(ex);
        }
        //设定自动心跳
        s.sendHeartbeatAuto(timeOut);
    }
}
