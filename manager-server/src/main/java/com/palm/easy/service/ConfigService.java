package com.palm.easy.service;


import com.alibaba.fastjson2.JSON;
import com.palm.easy.ManagerConfiguration;
import com.palm.easy.util.StringUtil;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Component;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
@Component
public class ConfigService {

    private Map<String,String> configs;
    private File configFile;
    public ConfigService(){
        String basePath = ManagerConfiguration.localPath();
        configFile=new File(basePath,"conf/configs.json");
        if(configFile.exists()){
            configs= (Map<String, String>) JSON.parse(StringUtil.readFrom(configFile));
        }else{
            configFile.getParentFile().mkdirs();
            configs=new HashMap<>();
        }
        if(StringUtil.isEmpty(configs.get("manager"))){
            String manager="http://"+ManagerConfiguration.localIp()+":"+ Solon.cfg().serverPort();
            saveConfig("manager",manager);
        }
    }
    public String getConfig(String name){
        return configs.get(name);
    }
    public String getConfig(String name,String defaultValue){
        String v=getConfig(name);
        if(StringUtil.isEmpty(v)){
            return defaultValue;
        }
        return v;
    }
    public boolean saveConfig(String name,String value){
        configs.put(name,value);

        StringUtil.write(configFile, JSON.toJSONString(configs));
        return true;
    }
    public boolean saveAll(Map<String,String> configs){
        this.configs=configs;
        StringUtil.write(configFile,JSON.toJSONString(configs));
        return true;
    }
    public Map<String,String> getConfigs(){
        return configs;
    }
}
