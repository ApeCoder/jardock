package com.palm.easy;

import com.palm.easy.util.Util;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Configuration;


@Configuration
public class ManagerConfiguration {
    public static String localPath(){
       return Solon.cfg().get("local.path","manager/");
    }
    public static String localIp(){
        return Solon.cfg().get("local.ip", Util.getLocalAddress());
    }

    public static String managerAddress(){
        return Solon.cfg().get("manager.address");
    }
    public static String managerToken(){
        return Solon.cfg().get("manager.token","");
    }

}
