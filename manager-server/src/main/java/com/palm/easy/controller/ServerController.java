package com.palm.easy.controller;

import com.palm.easy.actions.server.*;
import com.palm.easy.actions.service.*;
import com.palm.easy.anno.Auth;
import com.palm.easy.domain.FileData;
import com.palm.easy.domain.LogPart;
import com.palm.easy.domain.Service;
import com.palm.easy.exception.BusinessException;
import com.palm.easy.service.ConfigService;
import com.palm.easy.service.IServer;
import com.palm.easy.service.ServerManageService;
import com.palm.easy.util.StringUtil;
import org.jutils.jprocesses.model.ProcessInfo;
import org.noear.solon.Utils;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Result;
import org.noear.solon.core.handle.UploadedFile;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

@Auth
@Controller
@Mapping("server")
public class ServerController {
    @Inject
    ServerManageService manageService;
    @Inject
    ConfigService configService;


    @Mapping("names")
    public Result<Collection<String>> serverNames() {
        return Result.succeed(manageService.serverNames());
    }

    @Get
    @Post
    @Mapping("list_services")
    public Result<List<Service>> listServices() {
        return Result.succeed(manageService.listServices());
    }

    @Get
    @Post
    @Mapping("list_servers")
    public Result<Set<String>> listServers() {
        return Result.succeed(manageService.serverNames());
    }

    @Post
    @Mapping("add_service")
    public Result addService(Service service) {
        if (service == null) {
            return Result.failure("services不能为空");
        }
        if (StringUtil.isEmpty(service.getServer())) {
            return Result.failure("server不能为空");
        }
        IServer server = manageService.getServer(service.getServer());
        if (server == null) {
            return Result.failure("server不存在");
        }
        server.execute(new AddService(service));
        return Result.succeed();
    }

    @Post
    @Mapping("save_service")
    public Result saveService(Service service) {
        if (service == null) {
            return Result.failure("services不能为空");
        }
        if (StringUtil.isEmpty(service.getServer())) {
            return Result.failure("server不能为空");
        }
        IServer server = getServer(service.getServer());
        server.execute(new SaveService(service));//.saveService(service);
        return Result.succeed();
    }

    @Post
    @Mapping("del_service")
    public Result delService(Service service) {
        IServer server = getServer(service.getServer());
        server.execute(new DeleteService(service));
        return Result.succeed();
    }

    @Post
    @Mapping(path="service_upload",multipart = true)
    public Result service_upload(Service service, Context ctx) throws Exception {
        IServer server = getServer(service.getServer());
        UploadedFile file = ctx.file("file");
        if (file == null) {
            return Result.failure("未上传文件");
        }

        byte[] data = Utils.transferToBytes(file.content);
        file.content.close();
       // UploadFile uf=new UploadFile();
        Update update=new Update();
        update.setService(service);
        update.setConfig(configService.getConfigs());
        update.setFile(data);
        server.execute(update);
       // server.uploadFile(service,configService.getConfigs(), data);
        return Result.succeed();
    }

    @Post
    @Mapping("start_service")
    public Result start(Service service) {
        IServer server = getServer(service.getServer());

       // server.startService(service,configService.getConfigs());
        server.execute(new StartService(service.getName(),configService.getConfigs()));
        return Result.succeed();
    }

    @Post
    @Mapping("stop_service")
    public Result stop(Service service) {
        IServer server = getServer(service.getServer());
        server.execute(new StopService(service.getName()));
        return Result.succeed();
    }

    @Get
    @Mapping("get_log")
    public Result<LogPart> getLog(String service,String server, long pos) {
        IServer theServer = getServer(server);
        LogPart part = theServer.execute(new GetLogPart(service, pos));
        return Result.succeed(part);
    }

    @Mapping("list_file")
    public Result listFile(String serverName, String path) {
        IServer server = getServer(serverName);
        return Result.succeed(server.execute(new ListFile(path)));
    }

    @Mapping("remove_file")
    public Result removeFile(String serverName, String path) {
        IServer server = getServer(serverName);
        if (server.execute(new DeleteFile(path))) {
            return Result.succeed("已删除");
        }
        return Result.failure("删除失败");
    }
    @Mapping("rename")
    public Result rename(String serverName, String path,String newName) {
        IServer server = getServer(serverName);
        if (server.execute(new FileReName(path,newName))) {
            return Result.succeed("已重命名");
        }
        return Result.failure("文件重命名失败");
    }
    @Mapping("export_file")
    public Result exportFile(String serverName, String path, Context ctx) {
        IServer server = getServer(serverName);

        FileData data = server.execute(new ExportFile(path));
        if (data == null) {
            return Result.failure("导出失败");
        }
        ctx.setHandled(true);
        ctx.headerAdd("Content-Type", "application/download");
        try {
            ctx.headerAdd("Content-Disposition", "attachment;filename=" + URLEncoder.encode(data.getName(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ctx.output(data.getData());
        return null;
    }

    @Mapping(path = "upload_file",multipart=true)
    public Result upload(String serverName, String path, Context ctx) throws Exception {
        IServer server = getServer(serverName);
        UploadedFile uf = ctx.file("file");
        if (uf == null) {
           return Result.failure("上传文件失败");
        }
        UploadFile uploadFile=new UploadFile();
        uploadFile.setPath(path);
        uploadFile.setName(uf.name);
        uploadFile.setData(Utils.transferToBytes(uf.content));
        server.execute(uploadFile);//.uploadServerFile(path, fd);
        return Result.succeed("上传完成");
    }

    @Mapping("unzip_file")
    public Result unZip(String serverName, String path) {
        IServer server = getServer(serverName);
        return server.execute(new UnzipFile(path)) ? Result.succeed("已解压") : Result.failure("解压失败");
    }

    @Mapping("load_file")
    public Result loadFile(String serverName, String path) {
        IServer server = manageService.getServer(serverName);
        return Result.succeed(server.execute(new LoadStringFile(path)));
    }

    @Mapping("save_file")
    public Result saveFile(String serverName, String path, String file) {
        IServer server = getServer(serverName);
        server.execute(new SaveStringFile(path,file));
        return Result.succeed();
    }
    @Mapping("new_file")
    public Result newFile(String serverName, String path, String file){
        IServer server = getServer(serverName);
        server.execute(new NewFile(path,file));
        return Result.succeed();
    }
    @Mapping("new_dir")
    public Result newDir(String serverName, String path, String file){
        IServer server = getServer(serverName);
        server.execute(new MakeDir(path,file));
        return Result.succeed();
    }
    @Mapping("status")
    public Result status(String server){
        IServer the_server = getServer(server);
        Map<String,Object> status=the_server.execute(new GetServerStatus());
        return Result.succeed(status);
    }
    @Mapping("sevice_status")
    public Result serviceStatus(Service service){
        IServer the_server = getServer(service.getServer());
        GetServiceStatus getStatus=new GetServiceStatus();
        getStatus.setServiceName(service.getName());
        Map<String,Object> status=the_server.execute(getStatus);
        if(status==null){
            return Result.failure("未获取到状态");
        }
        return Result.succeed(status);
    }
    @Mapping("sevice_process")
    public Result getProcessInfo(Service service){
        IServer server = getServer(service.getServer());
        ProcessInfo info = server.execute(new GetProcessInfo(service.getName()));
        return Result.succeed(info);
    }
    private IServer getServer(String serverName){
        IServer server = manageService.getServer(serverName);
        if(server==null){
            throw new BusinessException("未找到服务器 "+serverName);
        }
        return server;
    }
}
