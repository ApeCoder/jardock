package com.palm.cloud;

import org.java_websocket.client.WebSocketClient;
import org.noear.solon.core.message.Listener;
import org.noear.solon.core.message.Message;
import org.noear.solon.core.message.Session;

import java.io.IOException;

public class ClientListener implements Listener {
    @Override
    public void onMessage(Session session, Message message) throws IOException {
        System.out.println("客户端：我收到了：" + message.bodyAsString());
    }
    public void onClose(Session session) {
        WebSocketClient client = (WebSocketClient) session.real();
        if (client != null) {
            try {
                Thread.sleep(3000);
                client.reconnectBlocking();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
