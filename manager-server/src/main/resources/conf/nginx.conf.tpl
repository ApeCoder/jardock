#user  nobody;
worker_processes  #(processes);

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    gzip  on;

    map $http_upgrade $connection_upgrade {
        default upgrade;
        ''   close;
    }
    #for(service:services)
        #if(service.mappingServers.size()>1)
        upstream #(service.name) {
            hash $request_uri;
            #for(server:service.mappingServers)
            server #(server);
            #end
        }
    #end
    #end
    server {
        listen       80;
        server_name  localhost;
        server_tokens off;
        #charset koi8-r;

        #access_log  logs/host.access.log  main;
        #access_log  "pipe:rollback logs/host.access_log interval=1d baknum=7 maxsize=2G"  main;

        location / {
            root   #(root);
            index  index.html index.htm;
            more_clear_headers 'Server';
        }

        #for(service:services)
        location /#(service.name)/{
            proxy_pass #(service.protocol)://#(service.mappingServer)#if(service.mappingPath)#(service.mappingPath)#end;
            #if(service.mappingConfig)
            #(service.mappingConfig)
            #end
        }
        #end

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }

}
