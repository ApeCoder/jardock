package com.palm.easy.domain;

import java.io.Serializable;

public class LogPart implements Serializable {
    long last=0;
    String content="";

    public LogPart() {
    }

    public LogPart(long last, String content) {
        this.last = last;
        this.content = content;
    }

    public long getLast() {
        return last;
    }

    public void setLast(long last) {
        this.last = last;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
