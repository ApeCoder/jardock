import Vue from 'vue'
import axios from 'axios'
import HeyUI from 'HeyUI'

window.HeyUI = HeyUI

const loadComponent = (path) => {
    return new Promise((resolve, reject) => {
        require([path], resolve, reject)
    })
}
const dataCache={}

let ajax = {
    get: function (uri, params) {
        params = params || {};
        return axios.get(uri, {params: params});
    },
    delete: function (uri, params) {
        params = params || {};
        return axios.delete(uri, {params: params});
    },
    put: function (uri, params) {
        params = params || {};
        return axios.put(uri, params);
    },
    post: function (uri, params) {
        params = params || {};
        return axios.post(uri, params);
    },
    formPost: function (uri, params) {
        params = params || {};
        let data = new FormData();
        Object.keys(params).forEach(function (key) {
            let v = params[key];
            if (v == null) {
                return
            }
            if (v.constructor == Array) {
                for (let i = 0; i < v.length; i++) {
                    data.append(key, v[i]);
                }
            } else {
                data.append(key, v);
            }
        });
        return axios.post(uri, data);
    },
    upload: function (uri, params) {
        params = params || {};
        let data = new FormData();
        Object.keys(params).forEach(function (key) {
            let v = params[key];
            if (v == null) {
                return
            }
            if (v.constructor == Array) {
                for (let i = 0; i < v.length; i++) {
                    data.append(key, v[i]);
                }
            } else {
                data.append(key, v);
            }
        });
        return axios({
            url: uri,
            method: 'POST',
            data: data,
            onUploadProgress: function (progressEvent) {
                if (progressEvent.loaded == progressEvent.total) {
                    HeyUI.loading.close();
                } else {
                    let complete = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%'
                    //console.log('上传 ' + complete)
                    HeyUI.loading('文件上传中 ' + complete);
                }
            }
        });
    },
    download(path) {
        if (axios.defaults.baseURL) {
            path = axios.defaults.baseURL + path
        }
        let elemIF = document.createElement('iframe')
        if (path.indexOf('?') > 0) {
            path += "&_token=" + sessionStorage._token
        } else {
            path += "?_token=" + sessionStorage._token
        }
        elemIF.src = path;
        elemIF.style.display = 'none';
        elemIF.onload = function () {
            document.body.removeChild(elemIF);
        }
        document.body.appendChild(elemIF)
    },
    /**
     * 使用ajax方式下载
     * @param path 路径
     * @param fileName 文件下载后的名字
     * @param params 参数
     * @param method 请求方法,默认为GET
     */
    downloadA(path, fileName = "", params = {}, method = 'GET') {
        axios.request({url: path, method: method, params: params, responseType: 'arraybuffer'}).then(resp => {
            if (!fileName) {
                let tempName = resp.headers["content-disposition"];
                if (tempName) {
                    tempName = tempName.split(";")[1].split("filename=")[1];
                    fileName = decodeURI(tempName);
                } else {
                    fileName = "下载文件";
                }
            }
            let data = resp.data
            debugger
            //获取数据流数据
            let blob = new Blob([data]);
            var downloadElement = document.createElement("a");
            var href = window.URL.createObjectURL(blob); //创建下载的链接
            downloadElement.href = href;
            downloadElement.download = fileName; //下载后文件名
            document.body.appendChild(downloadElement);
            downloadElement.click(); //点击下载
            document.body.removeChild(downloadElement);//下载完成移除元素
            window.URL.revokeObjectURL(href);//释放掉blob对象
        })
    },
    loadModule(path) {//使用require载入模块，包含vue。区别与require的是，该方法不会缓存模块
        return new Promise(resolve => {
            require([path], function (md) {
                if (path.endsWith(".vue")) {
                    require.undef("vue-loader!" + path)
                } else {
                    require.undef(path)
                }
                resolve(md)
            })
        })
    },
    /**
     * 数据缓存
     * @param url
     * @param defaultValue
     * @returns {*}
     */
    data(url,defaultValue){
        let value= dataCache[url]
        if(value!=null){
            return value
        }
       value = Vue.ref(defaultValue)
        dataCache[url]=value
        Object.defineProperty(value,"load",{
            value(){
                ajax.get(url).then(r=>{
                    value.value=r.data
                })
            }
        })
        value.load()
        //Vue.ref(defaultValue)
        return value;
    },
    createDs(uri, pageAble = false) {
        let dataSource = Vue.reactive({
            data: [],
            params: {},
            loading: false,
            method: 'post',
            pagination: {
                page: 0,
                size: 10,
                total: 0
            }
        })
        let self = dataSource;
        //数据载入监听
        let listeners = {}
        let transformer = (v) => v
        Object.defineProperties(dataSource, {
            param: {
                value(params) {
                    Object.assign(dataSource.params, params)
                    return dataSource;
                }
            },
            setParam: {
                value(name, value) {
                    dataSource.params[name] = value;
                    return dataSource;
                }
            },
            on: {
                value(name, fn) {
                    if (listeners[name]) {
                        listeners[name].push(fn)
                    } else {
                        listeners[name] = [fn]
                    }
                }
            },
            clean: {
                value() {
                    dataSource.data = [];//.splice(0, self.data.length);
                    dataSource.pagination.total = 0
                }
            },
            putData: {
                value(data) {
                    self.data = data
                }
            },
            transform: {
                value(v) {
                    transformer = v
                }
            },
            load: {
                value(params, fn) {
                    return new Promise((resolve, reject) => {
                        self.loading = true;
                        if (params) {
                            if (params instanceof Function) {
                                fn = params;
                                params = self.params;
                            }
                        } else {
                            params = self.params;
                        }
                        if (self.pagination) {
                            let pagination = self.pagination
                            params.pageNo = pagination.page;
                            params.pageSize = pagination.size;
                        }
                        if (listeners.beforeLoad) {
                            for (let fn of listeners.beforeLoad) {
                                fn(params)
                            }
                        }
                        ajax[self.method](uri, params).then(resp => {
                            self.loading = false;
                            let total=0
                            let data=[]
                            if(resp instanceof Array){
                                total=resp.length
                                data=resp
                            }else{
                                total=resp.total||resp.recordCount
                                data=resp.data||resp.rows
                            }

                            if (listeners.load) {
                                for (let fn of listeners.load) {
                                    fn(data)
                                }
                            }

                            if (data) {
                                if (transformer) {
                                    data = transformer(data);
                                }
                                self.putData(data)
                                resolve({data, total:total })
                            }
                            if (pageAble && total) {
                                self.pagination.total = total;
                            }

                        }).catch(e => {
                            self.clean()
                            reject(e)
                            if (listeners.error) {
                                for (let fn of listeners.error) {
                                    fn(e)
                                }
                            }
                        })
                    })
                }
            }
        })

        return dataSource;
    },
}

function dateFormat(fmt, date) {
    var o = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours() % 12 == 0 ? 12 : date.getHours() % 12, //小时
        "H+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds() //毫秒
    };
    var week = {
        "0": "/u65e5",
        "1": "/u4e00",
        "2": "/u4e8c",
        "3": "/u4e09",
        "4": "/u56db",
        "5": "/u4e94",
        "6": "/u516d"
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    if (/(E+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "/u661f/u671f" : "/u5468") : "") + week[date.getDay() + ""]);
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;

}

function formatNumber(num, pattern) {
    var strarr = num ? num.toString().split('.') : ['0'];
    var fmtarr = pattern ? pattern.split('.') : [''];
    var retstr = '';
    // 整数部分
    var str = strarr[0];
    var fmt = fmtarr[0];
    var i = str.length - 1;
    var comma = false;
    for (var f = fmt.length - 1; f >= 0; f--) {
        switch (fmt.substr(f, 1)) {
            case '#':
                if (i >= 0) retstr = str.substr(i--, 1) + retstr;
                break;
            case '0':
                if (i >= 0) retstr = str.substr(i--, 1) + retstr;
                else retstr = '0' + retstr;
                break;
            case ',':
                comma = true;
                retstr = ',' + retstr;
                break;
        }
    }
    if (i >= 0) {
        if (comma) {
            var l = str.length;
            for (; i >= 0; i--) {
                retstr = str.substr(i, 1) + retstr;
                if (i > 0 && ((l - i) % 3) == 0) retstr = ',' + retstr;
            }
        } else retstr = str.substr(0, i + 1) + retstr;
    }
    retstr = retstr + '.';
    // 处理小数部分
    str = strarr.length > 1 ? strarr[1] : '';
    fmt = fmtarr.length > 1 ? fmtarr[1] : '';
    i = 0;
    for (var f = 0; f < fmt.length; f++) {
        switch (fmt.substr(f, 1)) {
            case '#':
                if (i < str.length) retstr += str.substr(i++, 1);
                break;
            case '0':
                if (i < str.length) retstr += str.substr(i++, 1);
                else retstr += '0';
                break;
        }
    }
    return retstr.replace(/^,+/, '').replace(/\.$/, '');
}

let format = function (value, format) {
    if (arguments.length == 2) {
        if (format.startsWith("#") || format.startsWith("0")) {
            return formatNumber(value, format);
        } else {
            if (value == null) {
                return "";
            }
            if (value instanceof Date) {

            } else {
                value = new Date(value);
            }
            return dateFormat(format, value);
        }
    }
    format = value;
    if (format.startsWith("#") || format.startsWith("0")) {
        return function (val) {
            return formatNumber(val, format);
        }
    } else {
        return function (val) {
            if (val == null) {
                return "";
            }
            if (val instanceof Date) {

            } else {
                val = new Date(val);
            }
            return dateFormat(format, val);
        }
    }

}

let DataSource = function (uri, pageable) {
    this.data = Vue.ref([]);
    this.params = Vue.reactive({});
    this.loading = false;
    this.uri = uri;
    this.method = "post"
    let self = this;
    if (pageable) {
        this.pagination = Vue.reactive({
            page: 0,
            size: 10,
            total: 0
        })
    }
    this.param = function (params) {
        Object.assign(self.params, params)
        return this;
    }
    this.setParam = function (name, value) {
        this.params[name] = value;
        return this;
    }
    //数据载入监听
    let listeners = {}
    /**
     * 数据载入监听
     * @param name beforeLoad load error
     * @param fn
     */
    this.on = function (name, fn) {
        if (listeners[name]) {
            listeners[name].push(fn)
        } else {
            listeners[name] = [fn]
        }
    }
    /**
     * 清空数据
     */
    this.clean = function () {
        self.data.value = [];//.splice(0, self.data.length);
    }
    /**
     * 把数据放入到data中,不直接通过data赋值,这样vue可以感知到数据变化
     * @param data
     */
    this.putData = function (data) {
        self.data.value = data
        // self.data.splice(0, self.data.length);
        // self.data.push(...data);
    }
    /**
     *
     * @param params 查询参数
     * @param fn 数据转换回调
     * @returns {DataSource}
     */
    this.load = function (params, fn) {
        self.loading = true;
        if (params) {
            if (params instanceof Function) {
                fn = params;
                params = self.params;
            }
        } else {
            params = self.params;
        }
        if (self.pagination) {
            let pagination = self.pagination
            params.pageNo = pagination.page;
            params.pageSize = pagination.size;
        }
        if (listeners.beforeLoad) {
            for (let fn of listeners.beforeLoad) {
                fn(params)
            }
        }

        ajax[self.method](self.uri, params).then(resp => {
            self.loading = false;
            if (listeners.load) {
                for (let fn of listeners.load) {
                    fn(resp.data)
                }
            }
            if (resp.data) {
                let data = resp.data;
                if (fn) {
                    data = fn(data);
                }
                self.putData(data)
            }
            if (self.pagination && resp.total) {
                self.pagination.total = resp.total;
            }
        }).catch(e => {
            self.clean()
            if (listeners.error) {
                for (let fn of listeners.error) {
                    fn(e)
                }
            }
        })
        return this;
    }
}

//init vue
let asyncComp = (path) => Vue.defineAsyncComponent(() => loadComponent(path))
const getDict=requirejs.s.contexts._.config.getDict
const asyncDicts={}
const createAsyncDict=(key)=>{
    const aDict=Vue.ref([])
    const v=Vue.ref('')
    Object.defineProperty(aDict,'mapping',{
        value(value,connector){
          //return  getDict(key)
            return v
        }
    })
    return aDict
}
function useHeyUI(instance) {
    const config=HeyUI.heyuiConfig
    const dictMapping=config.dictMapping
    config.dictMapping=function(value, key, connector){
        if(value==null||value==''){
            return ''
        }
        let dict=config.getDict(key)
        if(!dict||(dict instanceof Array && dict.length==0)){
            //动态载入
            let asyncDict=asyncDicts[key]||createAsyncDict(key)
            return asyncDict.mapping(value,connector)
        }
       // console.log(connector)
        // let dict = config.getDict(key);
        // if (!dict || value==null || value === "")
        //     return "";
        // if (utils.isString(value) && connector) {
        //     value = value.split(connector);
        // }
        // if (!utils.isArray(value)) {
        //     value = [value];
        // }
        // const keyField = this.getOption("dict", "keyName");
        // const titleField = this.getOption("dict", "titleName");
        // if (utils.isArray(dict)) {
        //     dict = utils.toObject(dict, keyField);
        // }
        // let result = value.map((ele) => {
        //     if (utils.isObject(ele)) {
        //         return ele[titleField];
        //     }
        //     const d = dict[ele];
        //     if (utils.isObject(d)) {
        //         return d[titleField];
        //     }
        //     return d;
        // });
        // return result.filter((ele) => ele && ele !== "").join(connector || ", ");
        //config.getDict()
       return dictMapping.call(config,value,key,connector)
    }
    let tempComponent = instance.component
    let coms = {
        AutoComplete: true,
        Checkbox: true,
        DropdownMenu: true,
        Radio: true,
        Select: true,
        Steps: true,
        SwitchList: true,
        Tabs: true
    }

    instance.component = function (name, def) {
        if(coms[name]){
            if (def.props.dict) {
                //定义动态字典
               def.setup=function(props,context){
                   if(props.dict){
                       console.log(props.dict)
                       //HeyUI.heyuiConfig.getDict()
                       let data=[{key:'test',title:'aaa'}]
                       getDict(props.dict).then(d=>{
                           data.push(...d.data)
                       })
                       return {
                           datas:data
                       }
                   }
               }
            }
        } else if(name=='Table'){
            def.setup=function(props,ctx){
               Vue.onBeforeMount(()=>{
                   let ins= Vue.getCurrentInstance()
                   console.log(ins.props)
                   let props=ins.props;
                   if(props.columns.length>0){
                       console.log("xxxxxx")
                       return
                   }
                   // // ins.ctx.initColumns=function(){
                   // //
                   // // }
                   // let columns = [];
                   // console.log(ins.ctx.initColumns)
                   let columns = props.columns
                   if(ins.slots.default){
                       for (let slot of ins.slots.default()) {
                           let option = slot.type.name;
                           if (option === 'HTableItem') {
                               let itemProps=slot.props;
                               if(itemProps.dict){
                                   delete itemProps.dict
                                   itemProps.render=function(){
                                       return "哈哈"
                                   }
                               }
                               columns.push(itemProps);
                           }
                       }
                   }
                    ins.ctx.columns= columns;
                   // props.columns=columns
                   // console.log(props.columns)
               })
            }

        }

        return tempComponent(name, def)
    }
    instance.use(HeyUI)
    instance.component=tempComponent
}

function regComponents(instance) {
    let Props = instance.config.globalProperties

    //useHeyUI(instance)
    instance.use(HeyUI)
   // instance.use(HeyUI)

    Props.$axios = axios
    Props.$ajax = ajax
//权限指令,uri
//     Vue.mixin({
//         props: {
//             priv: String,
//             role: String,
//         },
//         beforeCreate: function () {
//             let props = this.$props
//             if (props) {
//                 let _priv = props.priv
//                 if (_priv) {
//                     if (!this.$priv(_priv)) {
//                         this.$options = {//没有权限不渲染
//                             render: function () {
//                             }
//                         };
//                         return
//                     }
//                 }
//                 let _role = props.role
//                 if (_role) {
//                     if (!this.$role(_role)) {
//                         this.$options = {//没有权限不渲染
//                             render: function (c) {
//                             }
//                         };
//                         return
//                     }
//                 }
//             }
//             let uri = this.$attrs.uri
//             if (uri) {
//                 let ds = new DataSource(uri)
//                 props.datas = ds.data
//                 ds.load()
//                 //console.log(props)
//             }
//
//         }
//     })


    //导入组件
    instance.component('FileInput', asyncComp('coms/FileInput.vue'))
    instance.component('MonacoEditor', asyncComp('coms/MonacoEditor.vue'))
    instance.component('Chart', asyncComp('coms/Echarts.vue'))
    instance.component('RichTextEditor', asyncComp('coms/RichTextEditor.vue'))
    instance.component('Prompt', asyncComp('coms/Prompt.vue'))

    //兼容vue2.0的全局方法
    // Props.$Loading = HeyUI.loading
    // Props.$Message = HeyUI.message
    // Props.$Modal = HeyUI.modal
    // Props.$Notice = HeyUI.notice
    // Props.$Confirm = HeyUI.confirm
    // Props.$ImagePreview = HeyUI.imagePreview
    //Props.$Alert
    HeyUI.alert = function (title, info) {
        return new Promise(resolve => {
            HeyUI.modal({
                title: title,
                content: info,
                closeOnMask:false,
                middle:true,
                buttons: [{
                    type: 'ok',
                    name: '确认',
                    color: 'primary'
                }],
                events: {
                    ok: (modal) => {
                        modal.close();
                        resolve()
                    }
                }
            })
        })
    }
    //Props.$Prompt =
    /**
    HeyUI.prompt = function (title, defaultValue,type='Input') {
        let input='<Input style="width:100%" type="text" v-model="value"/>'
        switch (type){
            case 'Password':
                input='<Input style="width:100%" type="password" v-model="value"/>'
                break;
            case 'Date':
                input='<DatePicker style="width:100%" v-model="value"/>'
                break;
            case 'DateTime':
                input='<DatePicker style="width:100%" v-model="value" type="datetime"/>'
                break;
        }
        return new Promise(resolve => {
            HeyUI.modal({
                component: {
                    vue: {
                        data() {
                            return {
                                title: title,
                                value: defaultValue || ''
                            }
                        },
                        methods: {
                            cancel() {
                                this.$emit('close');
                            },
                            success() {
                                if (this.value) {
                                    resolve(this.value)
                                    this.$emit('close');
                                } else {
                                    HeyUI.message("没有内容")
                                }
                            }
                        },
                        template: '<div><header class="h-modal-header">{{title}}</header><div style="padding:10px">'+input+'</div><footer class="h-modal-footer"><Button @click="cancel">取消</Button><Button color="primary" @click="success">确认</Button></footer></div>',
                    }
                }
            })
        })

    }
    **/
}

function asGetter(propNameOrFun) {
    if (typeof propNameOrFun == 'function') {
        return propNameOrFun
    }
    return (it) => it[propNameOrFun]
}

const util = {
    /**
     * 数组转换为map
     * @param list 数组
     * @param getId
     * @param getValue
     * @returns {{}}
     */
    asMap(list, getId = (it, idx) => idx, getValue = it => it) {
        getId = asGetter(getId)
        getValue = asGetter(getValue)
        let map = {}
        for (let i = 0; i < list.length; i++) {
            let item = list[i]
            map[getId(item, i)] = getValue(item)
        }
        return map;
    },
    /**
     * 数组转换为树,parentId为0，false，null的或者获取不到parent的节点转为根节点
     * @param list
     * @param getId
     * @param getParentId
     * @param getValue
     * @returns {*[]}
     */
    asTree(list, getId = it => it, getParentId = it => null, getValue = it => it) {
        let ret = []
        getId = asGetter(getId)
        getParentId = asGetter(getParentId)
        getValue = asGetter(getValue)
        let map = util.asMap(list, getId, getValue)
        for (let it of list) {
            let parentId = getParentId(it)
            let _id = getId(it)
            let data = map[_id]
            if (!parentId) {
                ret.push(data)
                continue
            }
            let parent = map[parentId]
            if (parent) {
                if (!parent.children) {
                    parent.children = []
                }
                parent.children.push(data)
            } else {
                ret.push(data)
            }
        }
        return ret
    },
    fields(obj, fields) {
        let ret = {}
        if (obj == null || fields == null) {
            return ret;
        }
        for (let f of fields.split(',')) {
            if (obj.hasOwnProperty(f)) {
                ret[f] = obj[f]
            }
        }
        return ret;
    },
    fold(obj, fields) {//将对应字段的数组用','拼接
        if (obj == null || fields == null) {
            return obj;
        }
        if (!fields) {
            return obj;
        }
        let result = {};
        Object.assign(result, obj)
        for (let f of fields.split(',')) {
            if (obj[f]) {
                result[f] = obj[f].join(',')
            }
        }
        return result;
    },
    unfold(obj, fields) {//将对应字段的字符串用','拆封
        if (obj == null || fields == null) {
            return obj;
        }
        if (!fields) {
            return obj;
        }
        let result = {};
        Object.assign(result, obj)
        for (let f of fields.split(',')) {
            if (obj[f]) {
                result[f] = obj[f].split(',')
            }
        }
        return result;
    },
    mapToKv(data) {
        if (!data) {
            return []
        }
        let r = []
        let keys = Object.keys(data).sort((a, b) => a > b ? 1 : -1);
        for (let k of keys) {
            r.push({key: k, title: data[k]})
        }
        return r;
    },
    clean(value) {//清空数据
        if (value == null) {
            return;
        }
        if (value instanceof Array) {
            value.splice(0, value.length)
        } else {
            Object.keys(value).forEach(k => {
                let v = value[k]
                if (v == null) {
                    return
                }
                if (v instanceof Array) {
                    v.splice(0, v.length)
                } else {
                    value[k] = null
                }
            })
        }
    },
    format:format
};
let routed = {}
let app;
const framework = {
    version: '1.0',
    Vue,
    ajax,
    axios,
    loadComponent,
    hasPermission(code) {
        let store = Vuex.useStore()
        let user = store.state.user
        if (user.username == 'admin') {
            return true
        }
        if (!code) {
            return true
        }
        let permissions = user.permissions
        if (!permissions) {
            return false
        } else {
            let fun = new Function("p", "return "+code.replace(/([\w\.]+)/g, "p['$1']"))
            let pObj = {};
            permissions.forEach(it => pObj[it] = true)
            return fun(pObj)//permissions.indexOf(code) > -1
        }
    },
    //可以使用表达式 a.a&&(a.b||c.d)
    hasRole(code) {
        let store = Vuex.useStore()
        let user = store.state.user
        if (user.username == 'admin') {
            return true
        }
        if (!code) {
            return true
        }
        let roles = user.roles
        if (!roles) {
            return false
        } else {
            let fun = new Function("p","return " + code.replace(/([\w\.]+)/g, "p['$1']"))
            let pObj = {};
            roles.forEach(it => pObj[it] = true)
            return fun(pObj)//permissions.indexOf(code) > -1
            // return roles.indexOf(code) > -1
        }
    },
    util,
    HeyUI: HeyUI,
    format: format,
    DataSource: DataSource,
    init: function (idx, options) {
        if (options && options.baseURL) {
            axios.defaults.baseURL = options.baseURL
        }
        let initEvent = new Event('frameworkInit')
        initEvent.framework = framework
        dispatchEvent(initEvent)

        let idxPage = {
            template: '<RouterView/>'
        }
        Object.assign(idxPage, idx)
        app = Vue.createApp(idxPage);
        let perLoader = document.getElementById("per-loader");
        if (perLoader) {
            perLoader.style.display = 'none'
        }
        let createdEvent = new Event('appCreated')
        createdEvent.data = app
        dispatchEvent(createdEvent)
        // app.filter('format', format)
        regComponents(app)
        //app.mount(target)
        return app
    },
    mapState(params) {
        let store = Vuex.useStore()
        // return params.reduce((p,k)=>{
        //     p[k]=Vue.computed(()=>store.state[k])
        //     return p
        // },{})
        //  params.map(k=>Vue.computed(()=>store.state[k]))
        let states = Vuex.mapState(params)
        return Object.keys(states).reduce((p, k) => {
            p[k] = Vue.computed(states[k].bind({$store: store}))
            return p;
        }, {})
    },
    mapActions(actions) {
        let store = Vuex.useStore()
        return actions.reduce((p, k) => {
            p[k] = (data) => store.dispatch(k, data)
            return p
        }, {})
    },
    get app() {
        return app;
    }
}
export default framework

