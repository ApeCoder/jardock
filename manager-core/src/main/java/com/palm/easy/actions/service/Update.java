package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.Service;
import lombok.Data;

import java.util.Map;

@Data
public class Update implements IServerAction<Void> {
    private Service service;
    private byte[] file;
    private Map<String,String> config;
    @Override
    public Void execute(ServerContext context) {
        context.updateFile(service,config,file);
        return null;
    }
}
