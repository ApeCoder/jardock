package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.Service;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SaveService implements IServerAction<Void> {
    private Service service;

    @Override
    public Void execute(ServerContext context) {
        context.saveService(service);
        return null;
    }
}
