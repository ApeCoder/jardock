package com.palm.easy;



import com.jcraft.jsch.JSchException;

import com.palm.easy.util.Captcha;
import org.noear.solon.Solon;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.MethodType;
import org.noear.solon.core.handle.Result;


import javax.sql.DataSource;

import java.io.IOException;



@Configuration
public class ServerApp {
//    @Bean
//    public DataSource db(@Inject("${datasource}") cn.beecp.BeeDataSource ds) {
//        return ds;
//    }


    public static void main(String[] args) throws IOException, JSchException {
        //h2支持
       // DataSourceUtils.DBNameTypeMap.put("H22", DataSourceUtils.DBType.MYSQL);
        /**/
        Solon.start(ServerApp.class, args, app -> {
            // 用于集群反向部署
            app.enableWebSocket(true);
            //为了复用通道，不启用WebSocketD
            //  app.enableWebSocketD(true);
            // 验证码
            app.add("captcha", MethodType.GET, ctx -> Captcha.captcha(ctx));
            // 自动跳转首页
            app.add("/", MethodType.GET, ctx -> ctx.redirect("index.html"));
            // 500错误输出
            app.onError(e -> {
                Context ctx = Context.current();
                if (ctx != null) {
                    if (ctx.action() != null) {
                        try {
                            ctx.render(Result.failure(500, e.getMessage()));
                        } catch (Throwable ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            });

        });/**/
    }
}
