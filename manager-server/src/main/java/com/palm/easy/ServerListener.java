package com.palm.easy;

import com.palm.easy.anno.impl.AuthHandler;
import com.palm.easy.domain.Service;
import com.palm.easy.domain.UserInfo;
import com.palm.easy.service.IServer;
import com.palm.easy.service.ServerManageService;
import com.palm.easy.service.impl.RemoteServer;
import com.pty4j.PtyProcess;
import com.pty4j.PtyProcessBuilder;

import org.noear.solon.Solon;

import org.noear.solon.annotation.ServerEndpoint;
import org.noear.solon.core.Aop;
import org.noear.solon.core.ValHolder;
import org.noear.solon.core.message.Listener;
import org.noear.solon.core.message.Message;
import org.noear.solon.core.message.MessageFlag;
import org.noear.solon.core.message.Session;
import org.noear.solon.core.util.PrintUtil;
import org.noear.solon.socketd.ProtocolManager;
import org.noear.solon.socketd.RequestManager;
import org.noear.solon.socketd.SocketD;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@ServerEndpoint(value = "**")
public class ServerListener implements Listener {

    ServerManageService manageService;

    String token;

    public ServerListener() {
        Aop.getAsyn(ServerManageService.class, bw -> manageService = bw.get());
        token = Solon.cfg().get("manager.token", "");
    }

    @Override
    public void onOpen(Session session) {
        String path = session.path();
        String reqToken = session.param("token");

        if ("/server".equals(path)) {
            if (!token.equals(reqToken)) {
                try {
                    session.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }

            //agent 服务注册
            //session.sendAndCallback();

            //Server server = SocketD.create(session, HessianEncoder.instance, HessianDecoder.instance, Server.class);
            IServer server = new RemoteServer(session);
            String addr = session.param("name");//server.host();
            PrintUtil.blueln("connected" + addr);
            session.setAttachment(addr);
            IServer oldServer = manageService.getServer(addr);
            //把前一个踢掉
            if (oldServer != null && oldServer instanceof RemoteServer) {
                try {
                    ((RemoteServer) oldServer).getSession().close();
                } catch (IOException e) {

                }
                manageService.remove(addr);
            }
            manageService.regServer(addr, server);
        } else if ("/client".equals(path)) {
            if (!token.equals(reqToken)) {
                try {
                    session.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }
            //manager 客户端注册
        } else if ("/terminal".equals(path)) {
            String token = session.param("_token");
            UserInfo user = AuthHandler.loadUser(token);
            if (user == null) {
                try {
                    session.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }

            //  session.real();

            ValHolder<PtyProcess> vh = new ValHolder();
            session.setAttachment(vh);
            CompletableFuture cf = CompletableFuture.runAsync(() -> {
                String[] cmd = {"/bin/sh", "-l"};
                Map<String, String> env = new HashMap<>(System.getenv());
                env.put("TERM", "xterm");
                PtyProcess process = null;
                try {
                    process = new PtyProcessBuilder()
                            .setCommand(cmd)
                            .setEnvironment(env)
                            .setRedirectErrorStream(true)
                            // .setConsole(true)
                            .start();
                    vh.value = process;
                    InputStream is = process.getInputStream();
                    byte[] buffer = new byte[1024];

                    while (true) {
                        if (!process.isAlive()) {
                            session.close();
                            return;
                        }
                        int i = is.read(buffer);
                        if (i == -1) {
                            return;
                        }

                        byte[] data = new byte[i];
                        System.arraycopy(buffer, 0, data, 0, i);

                        session.send(Message.wrap(data));
                    }
                } catch (IOException e) {
                    try {
                        process.destroy();
                        session.close();
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                    throw new RuntimeException(e);
                }

            });


        } else {
            try {
                session.close();
                return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onMessage(Session session, Message message) throws IOException {
        String path = session.path();
        if ("/terminal".equals(path)) {
            ValHolder<PtyProcess> vh = session.getAttachment();
            vh.value.getOutputStream().write(message.body());
            return;
        }
        if ("/server".equals(path)) {
            if (message.flag() == MessageFlag.message) {
                Message rel = ProtocolManager.decode(ByteBuffer.wrap(message.body()));
                if (rel.flag() == MessageFlag.response) {
                    String key=rel.key();
                    CompletableFuture<Message> request = RequestManager.get(key);
                    //请求模式
                    if (request != null) {
                        RequestManager.remove(key);
                        request.complete(rel);
                        return;
                    }
                }
            }
            return;
        }
        if (message.flag() == MessageFlag.heartbeat) {
            //心跳包 跳过
        } else if (message.flag() == MessageFlag.response) {
            System.out.println("收到响应" + message);
            //响应消息，跳过

        } else {
            // System.out.println(message);
            // System.out.println(message.body());

        }

    }

    @Override
    public void onClose(Session session) {
        String path = session.path();
        if ("/terminal".equals(path)) {
            System.out.println("session close");
            ValHolder<PtyProcess> vh = session.getAttachment();
            try {
                vh.value.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            vh.value.destroy();
            return;
        }
        String addr = session.getAttachment();
        if (addr != null) {
            System.out.println("session " + addr + " close");
            manageService.remove(addr);
        }

    }
}
