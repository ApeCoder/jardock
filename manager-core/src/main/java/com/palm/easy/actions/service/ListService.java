package com.palm.easy.actions.service;

import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.ServerContext;
import com.palm.easy.domain.Service;
import com.palm.easy.util.Util;

import java.util.Collection;

public class ListService implements IServerAction<Collection<Service>> {
    @Override
    public Collection<Service> execute(ServerContext context) {
        Collection<Service> services= context.getServiceMap().values();
        for(Service service:services){
            if (service.isRunning() && service.getProcessId() != null) {
                boolean isRunning = Util.findProcess(service.getProcessId());
                service.setRunning(isRunning);
            } else {
                service.setRunning(false);
            }
        }
        return services;
    }
}
