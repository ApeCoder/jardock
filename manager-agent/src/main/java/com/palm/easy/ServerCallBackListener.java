package com.palm.easy;

import com.alibaba.fastjson2.JSONB;
import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.IServer;
import org.java_websocket.client.WebSocketClient;
import org.noear.solon.core.message.Listener;
import org.noear.solon.core.message.Message;
import org.noear.solon.core.message.MessageFlag;
import org.noear.solon.core.message.Session;

import java.io.IOException;

public class ServerCallBackListener implements Listener {
    private IServer localServer;
    public ServerCallBackListener(IServer local){
        localServer=local;
    }
    @Override
    public void onMessage(Session session, Message message) throws IOException {
        // System.out.println(message);
        if (message.flag() == MessageFlag.message) {
            try{
                IServerAction cmd= (IServerAction) JSONB.parse(message.body(), JSONReader.Feature.SupportAutoType);
                Object ret=localServer.execute(cmd);
                Message retMessage=Message.wrapResponse(message,JSONB.toBytes(ret, JSONWriter.Feature.WriteClassName));
              //  ytebdata= JSONB.toBytes(retMessage);
               session.sendAsync(retMessage);
                //System.out.println(message.key());
             //  Message retMsg=Message.wrap(JSONB.toBytes())//new Message(6,message.key(),JSONB.toBytes(ret, JSONWriter.Feature.WriteClassName));



            }catch (Exception e){
                session.sendAsync(Message.wrapResponse(message,JSONB.toBytes(e, JSONWriter.Feature.WriteClassName)));
            }
        }

    }
//    private void sendResponse(Session session,Object obj){
//        Message retMessage=Message.wrapResponse()
//        session.send(Message.wrap(JSONB.toBytes(message)));
//    }

    @Override
    public void onClose(Session session) {
        //断线自动重连
        WebSocketClient client = (WebSocketClient) session.real();
        if (client != null) {
            try {
                Thread.sleep(3000);
                client.reconnectBlocking();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
