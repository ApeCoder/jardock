package com.palm.easy.service;

import com.palm.easy.actions.IServerAction;

public interface IServer {
    <R> R execute(IServerAction<R> action);
}
