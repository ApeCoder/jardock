package com.palm.easy;


import com.palm.easy.service.impl.LocalServer;
import com.palm.easy.util.StringUtil;

import org.noear.solon.Solon;
import org.noear.solon.Utils;
import org.noear.solon.core.Aop;
import org.noear.solon.core.event.EventBus;

import org.noear.solon.core.message.Session;
import org.noear.solon.core.util.PrintUtil;
import org.noear.solon.socketd.SessionFactoryManager;
import org.noear.solon.socketd.SocketD;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.TimeUnit;

public class AgentApp {
    public static void main(String[] args) throws IOException {
        Solon.start(AgentApp.class, args,app->{
            app.enableWebSocketD(true);
        });

        String address = ManagerConfiguration.managerAddress();
        if (StringUtil.isEmpty(address)) {
            PrintUtil.error("没有配置manager.address，请在配置文件中配置或使用启动参数--manager.address=ip:port方式指定");
            return;
        }
        String token = ManagerConfiguration.managerToken();
        String name="c-"+ManagerConfiguration.localIp();
        String sessionAddress = "ws://" + address + "/server?token=" + token+"&name="+name;

        PrintUtil.blueln("connect manager to: " + sessionAddress);
       // Session session = SocketD.createSession(sessionAddress);
        Session session = SessionFactoryManager.create(URI.create(sessionAddress), true);
       // session.headerSet("host",);

        LocalServer localServer= Aop.get(LocalServer.class);

        session.listener(new ServerCallBackListener(localServer));

        //心跳连接
        session.sendHeartbeat();
        Utils.scheduled.scheduleWithFixedDelay(() -> {
            try {
                if (session.isValid())
                    session.sendHeartbeat();
            } catch (Throwable ex) {
                EventBus.push(ex);
            }
        }, 10, 10, TimeUnit.SECONDS);
    }
}
