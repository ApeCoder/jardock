package com.palm.easy.service.impl;

import com.alibaba.fastjson2.JSONB;
import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.palm.easy.actions.IServerAction;
import com.palm.easy.service.IServer;
import com.palm.easy.util.IDUtil;
import org.noear.solon.core.message.Message;
import org.noear.solon.core.message.MessageFlag;
import org.noear.solon.core.message.Session;
import org.noear.solon.socketd.ProtocolManager;

import java.nio.ByteBuffer;

public class RemoteServer implements IServer {
    private Session session;
    public RemoteServer(Session session){
        this.session=session;
    }

    public Session getSession(){
        return session;
    }
    @Override
    public <R> R execute(IServerAction<R> action) {

        Message msg=new Message(MessageFlag.message, IDUtil.base64UUID(),JSONB.toBytes(action, JSONWriter.Feature.WriteClassName));
        ByteBuffer bf= ProtocolManager.encode(msg);
        Message message = session.sendAndResponse(new Message(MessageFlag.message,msg.key(),bf.array()));
        if(message.body()==null||message.body().length==0){
            return null;
        }
        Object obj=JSONB.parse(message.body(), JSONReader.Feature.SupportAutoType);
        if(obj instanceof RuntimeException){
            throw (RuntimeException)obj;
        }else if(obj instanceof Throwable){
            throw new RuntimeException((Throwable) obj);
        }
        return (R)obj;
    }
}
