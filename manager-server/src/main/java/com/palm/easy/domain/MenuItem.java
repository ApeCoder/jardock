package com.palm.easy.domain;


public class MenuItem extends Item {
    public static final int TYPE_MENU=0;//菜单
    public static final int TYPE_PERMISSION=1;//权限
    private String parent;
    private String icon;
    private String path;
    private int type=TYPE_MENU;
    public MenuItem(){

    }
    //拷贝构造
    public MenuItem(MenuItem item){
        this(item.getKey(),item.getTitle(),item.getIcon(),item.getOrder());
        this.setParent(item.getParent());
        this.setType(item.getType());
        this.setPath(item.getPath());
    }
    public MenuItem(String key, String title){
        super(key,title);
    }
    public MenuItem(String key, String title, String icon, int order) {
        super(key, title);
        this.icon = icon;
        this.setOrder(order);
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
